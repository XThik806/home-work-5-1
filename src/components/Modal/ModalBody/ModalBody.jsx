import React from 'react';
import PropTypes from 'prop-types';

const ModalBody = ({children, onClick = () => {}}) => {
    

    return (
        <>
            <div className="modalBody" >{children}</div>
        </>
    );
}


export default ModalBody;

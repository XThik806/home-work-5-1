import PropTypes from 'prop-types';

const Button = ({ type = "button", className = "btn", onClick = () => { }, children }) => {

    return (
        <button type="button" className="btn" onClick={onClick}>{children}</button>
        // <button className={classNames} onClick={onClick}>Open first modal</button>
        // <button className='btn' onClick={openSecondModal}>Open second modal</button>
    );


};

Button.propTypes = {
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    classNames: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node,
}
export default Button;